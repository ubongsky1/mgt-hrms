﻿using HrmsMgtApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrmsMgtApp.Services
{
    public class LeavesService : ILeaves
    {
        private readonly ApplicationDbContext _context;

        public LeavesService(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task Create(Leave model)
        {
            _context.Add(model);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var result = GetLeavess(id);
            _context.Remove(result);
          await  _context.SaveChangesAsync();
        }

        public IEnumerable<Leave> GetAllLeaves()
        {
            return
                _context.Leavesss;
        }

        public IEnumerable<Leave> GetAllLeaves(ApplicationUser model)
        {
            var s = _context.Leavesss.Where(userleave => userleave.EmployeeId == model.Id);
            return s;
        }

        public IEnumerable<Leave> GetAllLeavesHR()
        {

            var s = _context.Leavesss.Where(userApp => userApp.UHeadStatus == true);
            return s;
        }

        public IEnumerable<Leave> GetAllLeavesUH(ApplicationUser model)
        {
            var s = _context.Leavesss.Where(userDept => userDept.UnitsId == model.UnitId);
            return s;
        }

        public Leave GetLeavess(int id)
        {
          var s=  _context.Leavesss.Where(leaveid => leaveid.Id == id).FirstOrDefault();
            return s;
        }

        public bool IsApplyAlready(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public async Task Update(Leave model)
        {
            _context.Update(model);
          await  _context.SaveChangesAsync();
        }
    }
}
