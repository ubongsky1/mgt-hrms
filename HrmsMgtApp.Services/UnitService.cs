﻿using HrmsMgtApp.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrmsMgtApp.Services
{
    public class UnitService : IUnit
    {
        private readonly ApplicationDbContext _context;

        public UnitService(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task Create(Unit unit)
        {
            _context.Add(unit);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var s = GetUnit(id);
            _context.Remove(s);
            await _context.SaveChangesAsync();
        }

        public async Task Edit(Unit model)
        {
            _context.Update(model);
          await  _context.SaveChangesAsync();
        }

        public IEnumerable<Unit> GetAllUnits()
        {
            return
                _context.Units.Include(users=>users.Users);
        }

        public Unit GetUnit(int id)
        {
            var s = _context.Units.Where(unit => unit.Id == id).Include(user=>user.Users).FirstOrDefault();
            return s;
        }
    }
}
