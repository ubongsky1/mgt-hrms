﻿using HrmsMgtApp.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrmsMgtApp.Services
{
    public class TaskServices : ITasks
    {
        private readonly ApplicationDbContext _context;

        public TaskServices(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Create(Tasks model)
        {
             _context.Add(model);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
          var s=  GetTasks(id);
            _context.Remove(s);
            await _context.SaveChangesAsync();
        }

        public IEnumerable<Tasks> GetAllTasks()
        {
            return
                _context.Taskss;
        }

        public IEnumerable<Tasks> GetAllTasksHead(ApplicationUser model)
        {
            return
            _context.Taskss.Where(taskHead => taskHead.AssignBy == model.UserName).Include(users=>users.User).OrderByDescending(x=>x.Id);
        }

        public IEnumerable<Tasks> GetAllTasksHR(ApplicationUser model)
        {
            return
            _context.Taskss.Where(x => x.AssignBy == model.UserName).Include(user => user.User).OrderByDescending(x => x.Id);
        }

        public IEnumerable<Tasks> GetAllTaskUser(ApplicationUser model)
        {
           return
            _context.Taskss.Where(users => users.EmployeeId == model.Id).OrderByDescending(x => x.Id);
        }

        public Tasks GetTasks(int id)
        {
            var s = _context.Taskss.Where(taskId => taskId.Id == id).Include(x=>x.User).FirstOrDefault();
            return s;
        }


        public async Task Update(Tasks model)
        {
            _context.Update(model);
           await _context.SaveChangesAsync();
        }
    }
}
