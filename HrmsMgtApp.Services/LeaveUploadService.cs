﻿using HrmsMgtApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrmsMgtApp.Services
{
    public class LeaveUploadService : ILeaveUpload
    {
        private readonly ApplicationDbContext _context;

        public LeaveUploadService(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task Create(LeaveUpload model)
        {
           _context.Add(model);
         await   _context.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var check = GetLeave(id);
            _context.Remove(id);
         await   _context.SaveChangesAsync();
            
        }

        public IEnumerable<LeaveUpload> GetAllLeaveUploads()
        {
            return
                  _context.LeaveUploads;
        }

        public LeaveUpload GetLeave(int id)
        {
            var s = _context.LeaveUploads.Where(leaveid => leaveid.Id == id).FirstOrDefault();

            return s;
        }

        public LeaveUpload GetLeaveUser(ApplicationUser Uname)
        {
            var s = _context.LeaveUploads.Where(userInfo => userInfo.FirstName.ToLower() == Uname.FirstName.ToLower() || userInfo.FirstName.ToLower() == Uname.LastName.ToLower())
                .FirstOrDefault();

            return s;
        }

        public async Task Update(LeaveUpload model)
        {
            _context.Update(model);
            await _context.SaveChangesAsync();
        }
    }
}
