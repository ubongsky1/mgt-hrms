﻿using HrmsMgtApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrmsMgtApp.Services
{
    public class EmployeeService : IEmployeeBio
    {
        private readonly ApplicationDbContext _context;

        public EmployeeService(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<EmployeeBioData> AllEmployeeBios()
        {
          return  _context.EmployeeBios;
        }

        public async Task Create(EmployeeBioData model)
        {
            _context.Add(model);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
          var s=  GetEmployeeBio(id);

            _context.Remove(s);
           await _context.SaveChangesAsync();
        }

        public EmployeeBioData GetEmployee(int id)
        {
            var s = _context.EmployeeBios.Where(emp => emp.EmployeeId == id).FirstOrDefault();
            return s;
        }

        public EmployeeBioData GetEmployeeBio(int id)
        {
            var s = _context.EmployeeBios.Where(emp => emp.Id == id).FirstOrDefault();
            return s;
        }

        public async Task Update(EmployeeBioData model)
        {
            _context.Update(model);
          await  _context.SaveChangesAsync();
        }
    }
}
