﻿using HrmsMgtApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrmsMgtApp.Services
{
    public class LeaveTypeService : ILeaveTypes
    {
        private readonly ApplicationDbContext _context;

        public LeaveTypeService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Create(LeaveType model)
        {
            _context.Add(model);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
          var check=  GetLeaveType(id);

            _context.Remove(check);

          await  _context.SaveChangesAsync();
        }

        public IEnumerable<LeaveType> GetAllLeaves()
        {
            return _context.LeaveTypes;
        }

        public LeaveType GetLeaveType(int id)
        {
          var s=  _context.LeaveTypes.Where(x => x.Id == id).FirstOrDefault();
            return s;
        }

        public async Task Update(LeaveType model)
        {
            _context.Update(model);
          await  _context.SaveChangesAsync();
        }
    }
}
