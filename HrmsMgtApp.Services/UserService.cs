﻿using HrmsMgtApp.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrmsMgtApp.Services
{
    public class UserService : IApplicationUser
    {
        private readonly ApplicationDbContext _context;

        public UserService(ApplicationDbContext context)
        {
            _context = context;
        }
        public Task Create(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ApplicationUser> GetAllUsers()
        {
            throw new NotImplementedException();
        }

        public ApplicationUser GetUser(int id)
        {
            var s = _context.Users.Where(user => user.Id == id).Include(userRole => userRole.Roles).ThenInclude(s=>s.Role)
                .Include(userUnit=>userUnit.Unit)
                .FirstOrDefault();
               
            return s;
        }

        public async Task Update(ApplicationUser model)
        {
            _context.Update(model);

            await _context.SaveChangesAsync();

        }
    }
}
