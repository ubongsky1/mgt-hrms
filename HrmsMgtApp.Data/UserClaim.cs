﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace HrmsMgtApp.Data
{
    public class UserClaim : IdentityUserClaim<int>
    {
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
