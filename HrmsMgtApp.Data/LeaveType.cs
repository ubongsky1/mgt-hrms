﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HrmsMgtApp.Data
{
   public class LeaveType
    {
        public int Id { get; set; }
        [Display(Name = "Leave Type")]
        [Required]
        public string Type { get; set; }

        [Display(Name = "Leave Type Description")]
        public string Description { get; set; }

        [Display(Name = "Created By")]
        public DateTime CreatedBy { get; set; } = DateTime.Now;

        [Display(Name = "Added By")]
        public string AddedBy { get; set; }

        [Display(Name = "Number of Days")]
        public int? NumberOfDays { get; set; }
    }
}
