﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HrmsMgtApp.Data
{
  public  interface IEmployeeBio
    {
        Task Create(EmployeeBioData model);
        EmployeeBioData GetEmployeeBio(int id);
        EmployeeBioData GetEmployee(int id);
        Task Update(EmployeeBioData model);
        Task Delete(int id);
        IEnumerable<EmployeeBioData> AllEmployeeBios();
    }
}
