﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HrmsMgtApp.Data
{
   public class Unit
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public ICollection<ApplicationUser> Users { get; set; }
    }
}
