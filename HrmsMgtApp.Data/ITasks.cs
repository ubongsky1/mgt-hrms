﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HrmsMgtApp.Data
{
   public interface ITasks
    {
        Task Create(Tasks model);
        Tasks GetTasks(int id);
        Task Update(Tasks model);
        Task Delete(int id);
        IEnumerable<Tasks> GetAllTasks();
        IEnumerable<Tasks> GetAllTasksHead(ApplicationUser model);
        IEnumerable<Tasks> GetAllTasksHR(ApplicationUser model);
        IEnumerable<Tasks> GetAllTaskUser(ApplicationUser model);
    }
}
