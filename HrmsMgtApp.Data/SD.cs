﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HrmsMgtApp.Data
{
    public static class SD
    {
        public const string SuperAdmin = "SuperAdmin";
        public const string Admin = "Admin";
        public const string User = "User";
        public const string HR = "HR";
        public const string UnitHead = "UnitHead";

    }
}
