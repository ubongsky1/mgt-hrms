﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrmsMgtApp.Data
{
    public class DummyData
    {
        public static async Task Initialize(ApplicationDbContext context,
                              UserManager<ApplicationUser> userManager,
                              RoleManager<ApplicationRole> roleManager)
        {
            context.Database.EnsureCreated();


            if (context.Units.Any())
            {
                return;
            }

            var unit = new List<Unit>() { 
            
             new Unit(){Name="IT",CreatedOn=DateTime.Now},
             new Unit(){Name="Data",CreatedOn=DateTime.Now},
              new Unit(){Name="Account",CreatedOn=DateTime.Now},
            };

            foreach (var u in unit)
            {
                context.Units.Add(u);
            }

            await  context.SaveChangesAsync();

            int adminId1 = 0;
            int adminId2 = 0;
            int adminId3 = 0;
            int adminId4 = 0;
            int adminId5 = 0;
            int adminId6 = 0;
            int adminId7 = 0;
            int adminId8 = 0;



            string role1 = "Admin";
            string desc1 = "This is the administrators role";

            string role2 = "User";
            string desc2 = "This is the users role";

            string role3 = "HR";
            string desc3 = "This is the HR role";

            string role4 = "UnitHead";
            string desc4 = "This is the UnitHead role";

            string role5 = "SuperAdmin";
            string desc5 = "This is the SuperAdmin role";


            string password = "P@$$w0rd";

            if (await roleManager.FindByNameAsync(role1) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role1, desc1, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role2) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role2, desc2, DateTime.Now));
            }

            if (await roleManager.FindByNameAsync(role3) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role3, desc3, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role4) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role4, desc4, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role5) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role5, desc5, DateTime.Now));
            }
            if (await userManager.FindByNameAsync("ubongsky1@gmail.com") == null)
            {
                var user = new ApplicationUser
                {

                    UserName = "ubongsky1@gmail.com",
                    Email = "ubongsky1@gmail.com",
                    PhoneNumber = "07082162958",
                    FirstName = "Ubong",
                    LastName = "Umoh",
                    EmailConfirmed = true,
                    UnitId=1
                };


                var result = await userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(user, password);
                    await userManager.AddToRoleAsync(user, role1);
                }
                adminId1 = user.Id;
            }




            if (await userManager.FindByNameAsync("akinbamidelet@gmail.com") == null)
            {
                var user = new ApplicationUser
                {

                    UserName = "akinbamidelet@gmail.com",
                    Email = "akinbamidelet@gmail.com",
                    PhoneNumber = "090123456789",
                    FirstName = "Admin",
                    LastName = "Test",
                    EmailConfirmed = true,
                    UnitId = 1

                };

                var result = await userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(user, password);
                    await userManager.AddToRoleAsync(user, role2);
                }
                adminId2 = user.Id;
            }




        }
    }
}
