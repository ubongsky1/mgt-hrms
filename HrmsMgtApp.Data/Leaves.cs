﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HrmsMgtApp.Data
{
   public class Leaves
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Leave Start Date")]
        public DateTime StartDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Leave End Date")]
        public DateTime EndDate { get; set; }

        [Display(Name = "Employee")]
        public int EmployeeId { get; set; }

        //[Required]
        [Display(Name = "Employee Email")]
        public string EmployeeEmail { get; set; }

        [Required]
        [Display(Name = "Leave Type")]
        [ForeignKey("LeaveType")]
        public int LeaveTypeId { get; set; }

        public virtual LeaveType LeaveType { get; set; }

        //[Required]
        [Display(Name = "Purpose of Leave Request")]
        public string Purpose { get; set; }

        //[Required]
        [Display(Name = "U.Head Approved?")]
        public bool isUnitHeadApproved { get; set; }

        //[Required]
        [Display(Name = "HR Approved?")]
        public bool isHrApproved { get; set; }

        [Display(Name = "U.Head Disapproved?")]
        public bool isUnitHeadDisapprove { get; set; }

        [Display(Name = "HR Disapproved?")]
        public bool isHrDisapprove { get; set; }
        //[Required]
        [Display(Name = "Total Leave")]
        public int TotalLeave { get; set; }
        //[Required]
        [Display(Name = "Leave Utilized")]
        public int LeaveUtilized { get; set; }
        //[Required]
        [Display(Name = "Leave Remaining")]
        public int LeaveRemaining { get; set; }
        //[Required]
        [Display(Name = "Leave Request Status")]
        public string LeaveRequestStatus { get; set; }

        [Required]
        [Display(Name = "Employee Unit")]
        [ForeignKey("Units")]
        public int UnitsId { get; set; }

        public virtual Unit Units { get; set; }

        [Display(Name = "Leave HandOver Note")]
        public string HandOver { get; set; }

     
        [Display(Name = "Handover Employee")]
        [ForeignKey("User")]
        public int? UserId { get; set; }

        [Display(Name ="Added By")]
        public string AddedBy { get; set; }

        [Display(Name = "Created Date")]
        public DateTime Created { get; set; } = DateTime.Now;

        public virtual ApplicationUser User { get; set; }

        [Display(Name = "Leave Deleted")]
        public bool isDeleted { get; set; }
    }
}
