﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HrmsMgtApp.Data.Migrations
{
    public partial class additionalfields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AddedBy",
                table: "EmployeeBios",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BloodGroup",
                table: "EmployeeBios",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "CUG",
                table: "EmployeeBios",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DOB",
                table: "EmployeeBios",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfEmployment",
                table: "EmployeeBios",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Designation",
                table: "EmployeeBios",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "EmployeeNo",
                table: "EmployeeBios",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MaritalStatus",
                table: "EmployeeBios",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModeOfEmployment",
                table: "EmployeeBios",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "YearsOfExperience",
                table: "EmployeeBios",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddedBy",
                table: "EmployeeBios");

            migrationBuilder.DropColumn(
                name: "BloodGroup",
                table: "EmployeeBios");

            migrationBuilder.DropColumn(
                name: "CUG",
                table: "EmployeeBios");

            migrationBuilder.DropColumn(
                name: "DOB",
                table: "EmployeeBios");

            migrationBuilder.DropColumn(
                name: "DateOfEmployment",
                table: "EmployeeBios");

            migrationBuilder.DropColumn(
                name: "Designation",
                table: "EmployeeBios");

            migrationBuilder.DropColumn(
                name: "EmployeeNo",
                table: "EmployeeBios");

            migrationBuilder.DropColumn(
                name: "MaritalStatus",
                table: "EmployeeBios");

            migrationBuilder.DropColumn(
                name: "ModeOfEmployment",
                table: "EmployeeBios");

            migrationBuilder.DropColumn(
                name: "YearsOfExperience",
                table: "EmployeeBios");
        }
    }
}
