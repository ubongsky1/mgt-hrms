﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HrmsMgtApp.Data.Migrations
{
    public partial class someChangesagain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "EmployeeBios",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "EmployeeBios",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "EmployeeBios");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "EmployeeBios");
        }
    }
}
