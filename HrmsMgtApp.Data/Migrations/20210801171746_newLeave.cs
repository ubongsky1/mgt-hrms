﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HrmsMgtApp.Data.Migrations
{
    public partial class newLeave : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Leavesss",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false),
                    EmployeeEmail = table.Column<string>(nullable: true),
                    LeaveTypeId = table.Column<int>(nullable: false),
                    Purpose = table.Column<string>(nullable: true),
                    UHeadStatus = table.Column<bool>(nullable: true),
                    HRStatus = table.Column<bool>(nullable: true),
                    Cancelled = table.Column<bool>(nullable: false),
                    TotalLeave = table.Column<int>(nullable: false),
                    LeaveUtilized = table.Column<int>(nullable: false),
                    LeaveRemaining = table.Column<int>(nullable: false),
                    LeaveRequestStatus = table.Column<string>(nullable: true),
                    UnitsId = table.Column<int>(nullable: false),
                    HandOver = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: true),
                    AddedBy = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    isDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Leavesss", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Leavesss_LeaveTypes_LeaveTypeId",
                        column: x => x.LeaveTypeId,
                        principalTable: "LeaveTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Leavesss_Units_UnitsId",
                        column: x => x.UnitsId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Leavesss_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Leavesss_LeaveTypeId",
                table: "Leavesss",
                column: "LeaveTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Leavesss_UnitsId",
                table: "Leavesss",
                column: "UnitsId");

            migrationBuilder.CreateIndex(
                name: "IX_Leavesss_UserId",
                table: "Leavesss",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Leavesss");
        }
    }
}
