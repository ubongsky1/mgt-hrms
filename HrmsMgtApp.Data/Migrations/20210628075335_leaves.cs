﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HrmsMgtApp.Data.Migrations
{
    public partial class leaves : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LeaveTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<DateTime>(nullable: false),
                    AddedBy = table.Column<string>(nullable: true),
                    NumberOfDays = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeaveTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LeaveUploads",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Surname = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    StaffNumber = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    NumberOfOutstandingLeaves = table.Column<string>(nullable: true),
                    CurrentLeaveDays = table.Column<string>(nullable: true),
                    TotalLeaves = table.Column<string>(nullable: true),
                    RemainingLeaves = table.Column<string>(nullable: true),
                    UtilizedLeaves = table.Column<string>(nullable: true),
                    AddedBy = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeaveUploads", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LeaveTypes");

            migrationBuilder.DropTable(
                name: "LeaveUploads");
        }
    }
}
