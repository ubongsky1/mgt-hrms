﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HrmsMgtApp.Data.Migrations
{
    public partial class EmployeeBio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EmployeeBios",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmployeeId = table.Column<int>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    CountryOfOrigin = table.Column<string>(nullable: true),
                    BeneficiaryName = table.Column<string>(nullable: true),
                    BeneficiaryNo = table.Column<string>(nullable: true),
                    BeneficiaryAddress = table.Column<string>(nullable: true),
                    EmergencyName = table.Column<string>(nullable: true),
                    EmergencyNo = table.Column<string>(nullable: true),
                    EmergencyAddress = table.Column<string>(nullable: true),
                    Lgas = table.Column<string>(nullable: true),
                    States = table.Column<string>(nullable: true),
                    Region = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Academic = table.Column<string>(nullable: false),
                    AcademicDescription = table.Column<string>(nullable: false),
                    Professional = table.Column<string>(nullable: true),
                    ProfessionalDescription = table.Column<string>(nullable: true),
                    Institution = table.Column<string>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    Duration = table.Column<int>(nullable: false),
                    StartDate = table.Column<string>(nullable: false),
                    EndDate = table.Column<string>(nullable: false),
                    Gender = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeBios", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmployeeBios");
        }
    }
}
