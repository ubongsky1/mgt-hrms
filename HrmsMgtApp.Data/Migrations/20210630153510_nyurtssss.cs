﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HrmsMgtApp.Data.Migrations
{
    public partial class nyurtssss : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Leaves",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false),
                    EmployeeEmail = table.Column<string>(nullable: true),
                    LeaveTypeId = table.Column<int>(nullable: false),
                    Purpose = table.Column<string>(nullable: true),
                    isUnitHeadApproved = table.Column<bool>(nullable: false),
                    isHrApproved = table.Column<bool>(nullable: false),
                    isUnitHeadDisapprove = table.Column<bool>(nullable: false),
                    isHrDisapprove = table.Column<bool>(nullable: false),
                    TotalLeave = table.Column<int>(nullable: false),
                    LeaveUtilized = table.Column<int>(nullable: false),
                    LeaveRemaining = table.Column<int>(nullable: false),
                    LeaveRequestStatus = table.Column<string>(nullable: true),
                    UnitsId = table.Column<int>(nullable: false),
                    HandOver = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: true),
                    AddedBy = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Leaves", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Leaves_LeaveTypes_LeaveTypeId",
                        column: x => x.LeaveTypeId,
                        principalTable: "LeaveTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Leaves_Units_UnitsId",
                        column: x => x.UnitsId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Leaves_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Leavess",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false),
                    EmployeeEmail = table.Column<string>(nullable: true),
                    LeaveTypeId = table.Column<int>(nullable: false),
                    Purpose = table.Column<string>(nullable: true),
                    isUnitHeadApproved = table.Column<bool>(nullable: false),
                    isHrApproved = table.Column<bool>(nullable: false),
                    isUnitHeadDisapprove = table.Column<bool>(nullable: false),
                    isHrDisapprove = table.Column<bool>(nullable: false),
                    TotalLeave = table.Column<int>(nullable: false),
                    LeaveUtilized = table.Column<int>(nullable: false),
                    LeaveRemaining = table.Column<int>(nullable: false),
                    LeaveRequestStatus = table.Column<string>(nullable: true),
                    UnitsId = table.Column<int>(nullable: false),
                    HandOver = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: true),
                    AddedBy = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Leavess", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Leavess_LeaveTypes_LeaveTypeId",
                        column: x => x.LeaveTypeId,
                        principalTable: "LeaveTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Leavess_Units_UnitsId",
                        column: x => x.UnitsId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Leavess_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Leaves_LeaveTypeId",
                table: "Leaves",
                column: "LeaveTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Leaves_UnitsId",
                table: "Leaves",
                column: "UnitsId");

            migrationBuilder.CreateIndex(
                name: "IX_Leaves_UserId",
                table: "Leaves",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Leavess_LeaveTypeId",
                table: "Leavess",
                column: "LeaveTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Leavess_UnitsId",
                table: "Leavess",
                column: "UnitsId");

            migrationBuilder.CreateIndex(
                name: "IX_Leavess_UserId",
                table: "Leavess",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Leaves");

            migrationBuilder.DropTable(
                name: "Leavess");
        }
    }
}
