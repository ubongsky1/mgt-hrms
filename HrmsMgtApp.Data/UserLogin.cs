﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace HrmsMgtApp.Data
{
    public class UserLogin : IdentityUserLogin<int>
    {
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
