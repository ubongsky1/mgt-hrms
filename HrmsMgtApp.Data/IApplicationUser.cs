﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HrmsMgtApp.Data
{
   public interface IApplicationUser
    {
        Task Create(ApplicationUser user);
        ApplicationUser GetUser(int id);
        Task Update(ApplicationUser model);
        Task Delete(int id);
        IEnumerable<ApplicationUser> GetAllUsers();
    }
}
