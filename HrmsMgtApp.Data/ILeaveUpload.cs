﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HrmsMgtApp.Data
{
   public interface ILeaveUpload
    {
        Task Create(LeaveUpload model);
        LeaveUpload GetLeave(int id);
        LeaveUpload GetLeaveUser(ApplicationUser Uname);
        Task Update(LeaveUpload model);
        Task Delete(int id);
        IEnumerable<LeaveUpload> GetAllLeaveUploads();

    }
}
