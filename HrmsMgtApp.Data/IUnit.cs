﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HrmsMgtApp.Data
{
   public interface IUnit
    {
        Task Create(Unit unit);
        Unit GetUnit(int id);
        Task Edit(Unit model);
        Task Delete(int id);
        IEnumerable<Unit> GetAllUnits();
    }
}
