﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HrmsMgtApp.Data
{
   public class LeaveUpload
    {
        public int Id { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string StaffNumber { get; set; }
        public string Location { get; set; }
        public string NumberOfOutstandingLeaves { get; set; }
        public string CurrentLeaveDays { get; set; }
        public string TotalLeaves { get; set; }
        public string RemainingLeaves { get; set; }
        public string UtilizedLeaves { get; set; }
        [Display(Name = "Added By")]
        public string AddedBy { get; set; }
        [Display(Name = "Created By")]
        public DateTime CreatedBy { get; set; } = DateTime.Now;
    }
}
