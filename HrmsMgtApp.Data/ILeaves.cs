﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HrmsMgtApp.Data
{
  public  interface ILeaves
    {
        Task Create(Leave model);
        Leave GetLeavess(int id);
        Task Update(Leave model);
        Task Delete(int id);
        IEnumerable<Leave> GetAllLeaves();
        IEnumerable<Leave> GetAllLeaves(ApplicationUser model);
        IEnumerable<Leave> GetAllLeavesUH(ApplicationUser model);
        IEnumerable<Leave> GetAllLeavesHR();

        bool IsApplyAlready(ApplicationUser user);
    }
}
