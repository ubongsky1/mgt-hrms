﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HrmsMgtApp.Data
{
    public enum Status
    {
        assigned,
        Ongoing,
        Completed,
        Pending
    }

    public class Tasks
    {
        public int Id { get; set; }

        [Display(Name = "Task")]
        public string TaskName { get; set; }

        [Display(Name = "Task Type")]
        public string TaskType { get; set; }

        [Display(Name = "Task Description")]
        public string TaskDescription { get; set; }

        [Display(Name = "Assign By")]
        public string AssignBy { get; set; }

        [Required]
        [Display(Name = "Assign To")]
        [ForeignKey("User")]
        public int EmployeeId { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }

        [Required]
        [Display(Name = "End Date")]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        public string Comment { get; set; }

        public string Remark { get; set; }

        [Required]
        public Status Status { get; set; }


        [Display(Name = "U.Head Status")]
        public bool? UHeadStatus { get; set; }

        [Display(Name = "HR. Status")]
        public bool? HRStatus { get; set; }


        [Display(Name = "EMp Approved?")]
        public bool? isEmpApproved { get; set; }

        public ApplicationUser User { get; set; }

        public DateTime Created { get; set; } = DateTime.Now;
    }
}
