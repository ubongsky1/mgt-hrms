﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HrmsMgtApp.Data
{
   public interface ILeaveTypes
    {
        Task Create(LeaveType model);
        LeaveType GetLeaveType(int id);
        Task Update(LeaveType model);
        Task Delete(int id);
        IEnumerable<LeaveType> GetAllLeaves();
    }
}
