﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Encodings.Web;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using HrmsMgtApp.Data;
using MimeKit;
using MimeKit.Text;
using MailKit.Net.Smtp;

namespace HrmsMgtApp.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ForgotPasswordModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailSender _emailSender;

        public ForgotPasswordModel(UserManager<ApplicationUser> userManager, IEmailSender emailSender)
        {
            _userManager = userManager;
            _emailSender = emailSender;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }
        }

        [Obsolete]
        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(Input.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return RedirectToPage("./ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please 
                // visit https://go.microsoft.com/fwlink/?LinkID=532713
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                var callbackUrl = Url.Page(
                    "/Account/ResetPassword",
                    pageHandler: null,
                    values: new { area = "Identity", code },
                    protocol: Request.Scheme);

                var message = new MimeMessage();
                //Setting the To e-mail address
                message.To.Add(new MailboxAddress(user.Email));


                //var tt = "ubsky1@gmail.com";
                //message.Bcc.Add(new MailboxAddress(tt));
                //Setting the From e-mail address
                message.From.Add(new MailboxAddress("E-mail From Hrms", "ubsky1@gmail.com"));
                //E-mail subject 
                message.Subject = "Forgot Password";
                //E-mail message body
                message.Body = new TextPart(TextFormat.Html)
                {
                    Text = "<img src='https://picsum.photos/200/300' width='400' height='100'>" +
                   "<br/><br/><br/><br/>" +
                "<p>Dear Colleague,</p>" +


               "Please reset password by <a href='" + callbackUrl + "'>Clicking here</a>" +




                                              "<br/>Regards<br/>" +


                                               "<img src='https://picsum.photos/200/300' width='400' height='100'>"
                };

                //Configure the e-mail
                using (var emailClient = new SmtpClient())
                {

                    emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    emailClient.Connect("smtp.gmail.com", 587, false);
                    emailClient.Authenticate("ubsky1@gmail.com", "PETROLEUM11@");
                    emailClient.Send(message);
                    emailClient.Disconnect(true);
                }


                await _emailSender.SendEmailAsync(
                    Input.Email,
                    "Reset Password",
                    $"Please reset your password by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                return RedirectToPage("./ForgotPasswordConfirmation");
            }

            return Page();
        }
    }
}
