﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using HrmsMgtApp.Data;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using MimeKit;
using MimeKit.Text;

namespace HrmsMgtApp.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly ApplicationDbContext _context;

        public RegisterModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender,ApplicationDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _context = context;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public class InputModel
        {


            [Required]
            [Display(Name = "First Name")]
            public string FirstName { get; set; }

            [Required]
            [Display(Name = "Last Name")]
            public string LastName { get; set; }


            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            [Display(Name = "Telephone")]
            public string PhoneNumber { get; set; }

            [Required]
            [Display(Name = "Employee Unit")]
            [ForeignKey("Units")]
            public int UnitsId { get; set; }
            
            public Unit Units { get; set; }

            [Display(Name = "Status")]
            public bool IsActive { get; set; }

            public DateTime CreatedOn { get; set; } = DateTime.Now;
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            ViewData["UnitsId"] = new SelectList(_context.Units, "Id", "Name");
            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
        }

        [Obsolete]
        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            ViewData["UnitsId"] = new SelectList(_context.Units, "Id", "Name", Input.UnitsId);
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {   FirstName=Input.FirstName,
                    LastName=Input.LastName,
                    UserName = Input.Email,
                    Email = Input.Email,
                    PhoneNumber=Input.PhoneNumber,
                    IsActive=Input.IsActive,
                    CreatedOn=Input.CreatedOn,
                    UnitId=Input.UnitsId
                };
                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    

                    string role = Request.Form["rdApplicationUserRole"].ToString();

                    if (role == SD.User)
                    {
                        await _userManager.AddToRoleAsync(user, SD.User);

                    }

                    _logger.LogInformation("User created a new account with password.");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { area = "Identity", userId = user.Id, code = code, returnUrl = returnUrl },
                        protocol: Request.Scheme);

                     var message = new MimeMessage();
                //Setting the To e-mail address
                message.To.Add(new MailboxAddress(user.Email));


                //var tt = "ubsky1@gmail.com";
                //message.Bcc.Add(new MailboxAddress(tt));
                //Setting the From e-mail address
                message.From.Add(new MailboxAddress("E-mail From Hrms", "ubsky1@gmail.com"));
                //E-mail subject 
                message.Subject = "Confirm Registration";
                //E-mail message body
                message.Body = new TextPart(TextFormat.Html)
                {
                    Text = "<img src='https://picsum.photos/200/300' width='400' height='100'>" +
                   "<br/><br/><br/><br/>" +
                "<p>Dear Colleague,</p>" +

               
               "Please confirm your Registration by <a href='" +callbackUrl + "'>Clicking here</a>" +




                                              "<br/>Regards<br/>" +


                                               "<img src='https://picsum.photos/200/300' width='400' height='100'>"
                };

                //Configure the e-mail
                using (var emailClient = new SmtpClient())
                {

                    emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    emailClient.Connect("smtp.gmail.com", 587, false);
                    emailClient.Authenticate("ubsky1@gmail.com", "PETROLEUM11@");
                    emailClient.Send(message);
                    emailClient.Disconnect(true);
                }


                    await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                        $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                    if (_userManager.Options.SignIn.RequireConfirmedAccount)
                    {
                        return RedirectToPage("RegisterConfirmation", new { email = Input.Email, returnUrl = returnUrl });
                    }
                    else
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        return LocalRedirect(returnUrl);
                    }
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
