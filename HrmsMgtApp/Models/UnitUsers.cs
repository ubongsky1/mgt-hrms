﻿using HrmsMgtApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HrmsMgtApp.Models
{
    public class UnitUsers
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public virtual IEnumerable<ApplicationUser> Users { get; set; }
    }
}
