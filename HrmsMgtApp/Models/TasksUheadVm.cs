﻿using HrmsMgtApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HrmsMgtApp.Models
{
    public class TasksUheadVm
    {
        public int ApprovedTask { get; set; }
        public int PendingTask { get; set; }
        public int RejectedTask { get; set; }
        public int TotalTasks { get; set; }
        public IEnumerable<Tasks> Alltasks { get; set; }
    }
}
