﻿using HrmsMgtApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HrmsMgtApp.Models
{
    public class AdminIndexVm
    {
       
        public IEnumerable<ApplicationUser> Users { get; set; }
        public IEnumerable<ApplicationRole> Roles { get; set; }
    }
}
