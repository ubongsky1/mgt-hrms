﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HrmsMgtApp.Models
{
    public enum Sex
    {
        Male, Female
    }

    public class EmployeeBioVm
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [Display(Name = "Full Name")]
        public string FullName
        {
            get
            {
                return FirstName + LastName;
            }
        }
        [Required]
        [Display(Name = "Blood Group")]
        public string BloodGroup { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date Of Birth")]
        public DateTime DOB { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string CUG { get; set; }

        [Display(Name = "Employee Code / No")]
        public string EmployeeNo { get; set; }
        [Display(Name = "Mode Of Employment")]
        public string ModeOfEmployment { get; set; }

        [DataType(DataType.Date), Display(Name = "Date Employed")]
        public DateTime DateOfEmployment { get; set; }

        [Display(Name = "Years Of Experience")]
        public string YearsOfExperience { get; set; }

        [Required]
        [Display(Name = "Employee Designation")]
        public string Designation { get; set; }

        [Display(Name = "Marital Status")]
        public string MaritalStatus { get; set; }
        [Display(Name = "Contact Address")]
        public string Address { get; set; }
        [Display(Name = "Nationality")]
        public string CountryOfOrigin { get; set; }

        [Display(Name = "Beneficiary's Name")]
        public string BeneficiaryName { get; set; }
        [Display(Name = "Phone Number")]
        public string BeneficiaryNo { get; set; }

        [Display(Name = "Contact Address")]
        public string BeneficiaryAddress { get; set; }

        [Display(Name = "Emergency Name")]
        public string EmergencyName { get; set; }
        [Display(Name = "Phone Number")]
        public string EmergencyNo { get; set; }

        [Display(Name = "Contact Address")]
        public string EmergencyAddress { get; set; }

        [Required]
        [Display(Name = "Local Govt Area")]
        public string Lgas { get; set; }

        [Required]
        [Display(Name = "State  Of Origin")]
        public string States { get; set; }

        [Display(Name = "Region")]
        public string Region { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Academic Qualification")]
        public string Academic { get; set; }


        [Display(Name = "Academic Qualification Description")]
        public string AcademicDescription { get; set; }

        [Display(Name = "Professional Qualification")]
        public string Professional { get; set; }

        [Display(Name = "Professional Qualification Description")]
        public string ProfessionalDescription { get; set; }

        [Required]
        [Display(Name = "Institution Attended")]
        public string Institution { get; set; }

        [Display(Name = "Certificate")]
        public IFormFile Image { get; set; }

        [Display(Name = "Duration")]
        public int Duration { get; set; }


        [Display(Name = "Start Date")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }


        [Display(Name = "Date Of Completion")]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        [Display(Name = "Added By")]
        public string AddedBy { get; set; }

        [Display(Name = "Date added")]
        public DateTime CreatedDate { get; set; } = DateTime.Now;

        public Sex Sex { get; set; }
    }
}
