﻿using HrmsMgtApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HrmsMgtApp.Models
{
    public class HrLeavesIndexVm
    {
        public int TotalRequests { get; set; }
        public int ApprovedRequests { get; set; }
        public int PendingRequests { get; set; }
        public int RejectedRequests { get; set; }
        public int TotalLeaves { get; set; }
        public IEnumerable<Leave> AllLeaves { get; set; }
    }
}
