﻿using HrmsMgtApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HrmsMgtApp.Models
{
    public class UserdetailsVm
    {
        public ApplicationUser User { get; set; }
        public EmployeeBioData BioData { get; set; }
    }
}
