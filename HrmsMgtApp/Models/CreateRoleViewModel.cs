﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HrmsMgtApp.Models
{
    public class CreateRoleViewModel
    {
        [Required]
        public string RoleName { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
    }
}
