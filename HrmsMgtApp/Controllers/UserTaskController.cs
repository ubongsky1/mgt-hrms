﻿using HrmsMgtApp.Data;
using HrmsMgtApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MimeKit;
using MimeKit.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;

namespace HrmsMgtApp.Controllers
{
    [Authorize]
    public class UserTaskController : Controller
    {
        private readonly ITasks _taskService;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserTaskController(ITasks taskService,SignInManager<ApplicationUser> signInManager,
                                                           UserManager<ApplicationUser> userManager)
        {
            _taskService = taskService;
            _signInManager = signInManager;
            _userManager = userManager;
        }
        // GET: UserTaskController
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var userId = user.Id;

           var s= _taskService.GetAllTaskUser(user);

            return View(s);
        }


        public async Task<IActionResult> TaskUhead()
        {
            var user = await _userManager.GetUserAsync(User);
            var s = _taskService.GetAllTasksHead(user);

            var info = new TasksUheadVm()
            {
                Alltasks=s,
                TotalTasks=s.Count(),
                ApprovedTask=s.Count(x=>x.UHeadStatus==true),
                PendingTask=s.Count(x=>x.UHeadStatus==null),
                RejectedTask=s.Count(x=>x.UHeadStatus==false)
            };

            return View(info);
        }

        public async Task<IActionResult> TaskUHr()
        {
            var user = await _userManager.GetUserAsync(User);
            var s = _taskService.GetAllTasksHR(user);

            var info = new TasksUheadVm()
            {
                Alltasks = s,
                TotalTasks = s.Count(),
                ApprovedTask = s.Count(x => x.HRStatus == true),
                PendingTask = s.Count(x => x.HRStatus == null),
                RejectedTask = s.Count(x => x.HRStatus == false)
            };

            return View(info);
        }

        // GET: UserTaskController/Details/5
        public IActionResult Details(int id)
        {
            var s = _taskService.GetTasks(id);
            if (s==null)
            {

                ViewBag.ErrorMessage = $"Task  with Id={id} cannot be found, Kindly contact the HR or Admin";

                return View("ErrorPage");
            }

            return View(s);
        }

        // GET: UserTaskController/Create
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var user = await _userManager.GetUsersInRoleAsync("User");
            var checkusers = user.Select(x => new
            {
                Id=x.Id,
                UserName = x.FirstName +' '+ x.LastName
            }).ToList();
            
            ViewData["Users"] = new SelectList(checkusers, "Id", "UserName");
            return View();
        }

        // POST: UserTaskController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Obsolete]
        public async Task<IActionResult> Create(Tasks model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var TaskCreator = await _userManager.GetUserAsync(User);
                    await _taskService.Create(model);

                    var sendTo = _userManager.Users.Where(userid => userid.Id == model.EmployeeId).Select(x => new { fullname = x.FirstName + ' ' + x.LastName, Email = x.UserName }).FirstOrDefault();

                    var EMailTo = sendTo.Email;
                    var fullName = sendTo.fullname.ToString();

                    var message = new MimeMessage();
                    //Setting the To e-mail address
                    message.To.Add(new MailboxAddress(fullName, EMailTo));

                    var tt = "ubong.umoh@junuvo.ng";
                    //var tt = "humanresources@novohealthafrica.org";
                    message.Bcc.Add(new MailboxAddress(tt));
                    //Setting the From e-mail address
                    message.From.Add(new MailboxAddress("E-mail From Hrms", "ubsky1@gmail.com"));
                    //E-mail subject 
                    message.Subject = "New Task Request Added";
                    //E-mail message body
                    message.Body = new TextPart(TextFormat.Html)
                    {
                        Text = "<img src='https://picsum.photos/200/300' width='400' height='100'>" +
                       "<br/><br/><br/><br/>" +
                    "<p>Dear Colleague,</p>" +

                   "<p> This is to notify you that a new Task request has been sent by " + TaskCreator.FirstName + " " + TaskCreator.LastName +

                   "<br/>on </a>" + model.Created + ".</p>" +
                   "Unit Head can see more information on the Task request by Clicking <a href='https://localhost:44307/UserTask/Details/" + model.Id + "'>Here</a>" +




                                                  "<br/>Regards<br/>" +


                                                   "<img src='https://picsum.photos/200/300' width='400' height='100'>"
                    };

                    //Configure the e - mail
                    using (var emailClient = new SmtpClient())
                    {

                        emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                        emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                        emailClient.Connect("smtp.gmail.com", 587, false);
                        emailClient.Authenticate("ubsky1@gmail.com", "PETROLEUM11@");
                        emailClient.Send(message);
                        emailClient.Disconnect(true);
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage= $"Kindly check your internet connection but the request has been sent";
                    return View("ErrorPage");
                }

                if (_signInManager.IsSignedIn(User)&&User.IsInRole("HR"))
                {
                    return RedirectToAction("TaskUHr", "UserTask");
                }
                else if (_signInManager.IsSignedIn(User) && User.IsInRole("UnitHead"))
                {
                    return RedirectToAction("TaskUhead", "UserTask");
                }
                else
                {
                    return RedirectToAction(nameof(Index));
                }
              
            }

            var user = await _userManager.GetUsersInRoleAsync("User");
            var checkusers = user.Select(x => new
            {
                Id = x.Id,
                UserName = x.FirstName + ' ' + x.LastName
            }).ToList();

            ViewData["Users"] = new SelectList(checkusers, "Id", "UserName",model.EmployeeId);


            return View(model);
        }

        // GET: UserTaskController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var check = _taskService.GetTasks(id);
            if (check==null)
            {
                ViewBag.ErrorMessage = $"Task  with Id={id} cannot be found, Kindly contact the HR or Admin";

                return View("ErrorPage");
            }

            var user = await _userManager.GetUsersInRoleAsync("User");
            var checkusers = user.Select(x => new
            {
                Id = x.Id,
                UserName = x.FirstName + ' ' + x.LastName
            }).ToList();

            ViewData["Users"] = new SelectList(checkusers, "Id", "UserName", check.EmployeeId);


            return View(check);
        }

        // POST: UserTaskController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Obsolete]
        public async Task<IActionResult> Edit(Tasks model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = await _userManager.GetUserAsync(User);
                    await _taskService.Update(model);
                    if (model.UHeadStatus==true)
                    {
                        var ToMess = _userManager.Users.Where(userid => userid.Id == model.EmployeeId).FirstOrDefault();

                        var TosenderEmail = ToMess.Email;
                        var TosenderName = ToMess.FirstName + ' ' + ToMess.LastName;

                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(TosenderName, TosenderEmail));

                        var tt = "ubong.umoh@junuvo.ng";
                        //var tt = "humanresources@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Hrms", "ubsky1@gmail.com"));
                        //E-mail subject 
                        message.Subject = "New Task Request Added";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://picsum.photos/200/300' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear Colleague,</p>" +

                       "<p> This is to notify you that your Task request has been Approved by " + user.FirstName + " " + user.LastName +

                       "<br/>on </a>" + model.Created + ".</p>" +
                       "Unit Head can see more information on the Task request by Clicking <a href='https://localhost:44307/UserTask/Details/" + model.Id + "'>Here</a>" +




                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://picsum.photos/200/300' width='400' height='100'>"
                        };

                        //Configure the e - mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.gmail.com", 587, false);
                            emailClient.Authenticate("ubsky1@gmail.com", "PETROLEUM11@");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }

                    }
                    else if (model.UHeadStatus==false)
                    {
                        var ToMess = _userManager.Users.Where(userid => userid.Id == model.EmployeeId).FirstOrDefault();

                        var TosenderEmail = ToMess.Email;
                        var TosenderName = ToMess.FirstName + ' ' + ToMess.LastName;

                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(TosenderName, TosenderEmail));

                        var tt = "ubong.umoh@junuvo.ng";
                        //var tt = "humanresources@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Hrms", "ubsky1@gmail.com"));
                        //E-mail subject 
                        message.Subject = "New Task Request Added";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://picsum.photos/200/300' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear Colleague,</p>" +

                       "<p> This is to notify you that your Task request has been Dissapproved by " + user.FirstName + " " + user.LastName +

                       "<br/>on </a>" + model.Created + ".</p>" +
                       "Unit Head can see more information on the Task request by Clicking <a href='https://localhost:44307/UserTask/Details/" + model.Id + "'>Here</a>" +




                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://picsum.photos/200/300' width='400' height='100'>"
                        };

                        //Configure the e - mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.gmail.com", 587, false);
                            emailClient.Authenticate("ubsky1@gmail.com", "PETROLEUM11@");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                    else if (model.HRStatus==true)
                    {
                        var ToMess = _userManager.Users.Where(userid => userid.Id == model.EmployeeId).FirstOrDefault();

                        var TosenderEmail = ToMess.Email;
                        var TosenderName = ToMess.FirstName + ' ' + ToMess.LastName;

                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(TosenderName, TosenderEmail));

                        var tt = "ubong.umoh@junuvo.ng";
                        //var tt = "humanresources@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Hrms", "ubsky1@gmail.com"));
                        //E-mail subject 
                        message.Subject = "New Task Request Added";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://picsum.photos/200/300' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear Colleague,</p>" +

                       "<p> This is to notify you that your Task request has been Approved by " + user.FirstName + " " + user.LastName +

                       "<br/>on </a>" + model.Created + ".</p>" +
                       "Unit Head can see more information on the Task request by Clicking <a href='https://localhost:44307/UserTask/Details/" + model.Id + "'>Here</a>" +




                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://picsum.photos/200/300' width='400' height='100'>"
                        };

                        //Configure the e - mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.gmail.com", 587, false);
                            emailClient.Authenticate("ubsky1@gmail.com", "PETROLEUM11@");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                    else if (model.HRStatus==false)
                    {
                        var ToMess = _userManager.Users.Where(userid => userid.Id == model.EmployeeId).FirstOrDefault();

                        var TosenderEmail = ToMess.Email;
                        var TosenderName = ToMess.FirstName + ' ' + ToMess.LastName;

                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(TosenderName, TosenderEmail));

                        var tt = "ubong.umoh@junuvo.ng";
                        //var tt = "humanresources@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Hrms", "ubsky1@gmail.com"));
                        //E-mail subject 
                        message.Subject = "New Task Request Added";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://picsum.photos/200/300' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear Colleague,</p>" +

                       "<p> This is to notify you that your Task request has been Dissapproved by " + user.FirstName + " " + user.LastName +

                       "<br/>on </a>" + model.Created + ".</p>" +
                       "Unit Head can see more information on the Task request by Clicking <a href='https://localhost:44307/UserTask/Details/" + model.Id + "'>Here</a>" +




                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://picsum.photos/200/300' width='400' height='100'>"
                        };

                        //Configure the e - mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.gmail.com", 587, false);
                            emailClient.Authenticate("ubsky1@gmail.com", "PETROLEUM11@");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                    //else if (model.UHeadStatus==null || model.HRStatus==null || model.UHeadStatus==false || model.HRStatus==false)
                    //{
                    //    return RedirectToAction(nameof(Index));
                    //}
                   
                }
                catch(Exception ex)
                {
                   var s= ex.Message;
                    ViewBag.ErrorMessage = $"Kindly check your internet connection but the request has been sent";
                    return View("ErrorPage");
                }

                if (_signInManager.IsSignedIn(User)&&User.IsInRole("HR"))
                {
                    return RedirectToAction("TaskUHr", "UserTask");
                }
                else if (_signInManager.IsSignedIn(User)&&User.IsInRole("UnitHead"))
                {
                    return RedirectToAction("TaskUhead", "UserTask");
                }
                else
                {
                    return RedirectToAction(nameof(Index));
                }

               
            }

            var users = await _userManager.GetUsersInRoleAsync("User");
            var checkusers = users.Select(x => new
            {
                Id = x.Id,
                UserName = x.FirstName + ' ' + x.LastName
            }).ToList();

            ViewData["Users"] = new SelectList(checkusers, "Id", "UserName", model.EmployeeId);
            return View(model);
        }

        // GET: UserTaskController/Delete/5
        public IActionResult Delete(int id)
        {
            var s = _taskService.GetTasks(id);
            if (s==null)
            {
                ViewBag.ErrorMessage = $"Leaves  with Id={id} cannot be found, Kindly contact the HR or Admin";

                return View("ErrorPage");
            }
            return View(s);
        }

        // POST: UserTaskController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(Tasks model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _taskService.Delete(model.Id);

                  
                }
                catch
                {
                    ViewBag.ErrorMessage = $"Issues inserting into the Db, Kindly contact the HR or Admin";

                    return View("ErrorPage");
                }

                if (_signInManager.IsSignedIn(User) && User.IsInRole("HR"))
                {
                    return RedirectToAction("TaskUHr", "UserTask");
                }
                else if (_signInManager.IsSignedIn(User) && User.IsInRole("UnitHead"))
                {
                    return RedirectToAction("TaskUhead", "UserTask");
                }
                return RedirectToAction(nameof(Index));
            }

            return View(model);
        }
    }
}
