﻿using HrmsMgtApp.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HrmsMgtApp.Controllers
{
    [Authorize]
    public class LeaveTypeController : Controller
    {
        private readonly ILeaveTypes _leaveTypesService;

        public LeaveTypeController(ILeaveTypes leaveTypesService)
        {
            _leaveTypesService = leaveTypesService;
        }
        // GET: LeaveTypeController
        public IActionResult Index()
        {
            var result = _leaveTypesService.GetAllLeaves();
            return View(result);
        }

        // GET: LeaveTypeController/Details/5
        public IActionResult Details(int id)
        {
            return View();
        }

        // GET: LeaveTypeController/Create
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        // POST: LeaveTypeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(LeaveType model)
        {
            if (ModelState.IsValid)
            {
               await _leaveTypesService.Create(model);

                return RedirectToAction(nameof(Index));

            }

            return View(model);
        }

        // GET: LeaveTypeController/Edit/5
        public IActionResult Edit(int id)
        {
          var result=  _leaveTypesService.GetLeaveType(id);
            if (result==null)
            {
                ViewBag.ErrorMessage = $"Leave Type with id = {id} cannot be found";
                return View("ErrorPage");
            }
            return View(result);
        }

        // POST: LeaveTypeController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(LeaveType model)
        {
            if (ModelState.IsValid)
            {
               
              await  _leaveTypesService.Update(model);
                return RedirectToAction(nameof(Index));

            }

            return View(model);
        }

        // GET: LeaveTypeController/Delete/5
        [HttpGet]
        public IActionResult Delete(int id)
        {
          var result=  _leaveTypesService.GetLeaveType(id);
            if (result==null)
            {
                ViewBag.ErrorMessage = $"Leave Type with id = {id} cannot be found";
                return View("ErrorPage");
            }
            return View(result);
        }

        // POST: LeaveTypeController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(LeaveType model)
        {
             await  _leaveTypesService.Delete(model.Id);

            return RedirectToAction(nameof(Index));

        }
    }
}
