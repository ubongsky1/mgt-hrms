﻿using HrmsMgtApp.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrmsMgtApp.Controllers
{
    [Authorize(Roles = "UnitHead, DeptHead, Admin, SuperAdmin,HR")]
    public class LeaveUploadController : Controller
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly ILeaveUpload _leaveUpload;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;

        public LeaveUploadController(IWebHostEnvironment hostingEnvironment, ILeaveUpload leaveUpload,
                                                UserManager<ApplicationUser> userManager,ApplicationDbContext context)
        {
            _hostingEnvironment = hostingEnvironment;
            _leaveUpload = leaveUpload;
            _userManager = userManager;
            _context = context;
        }
        // GET: LeaveUploadController
        public IActionResult Index()
        {

          var result=  _leaveUpload.GetAllLeaveUploads();
            return View(result);
        }


        public IActionResult OnPostImport()
        {
            IFormFile file = Request.Form.Files[0];
            string folderName = "Upload";
            string webRootPath = _hostingEnvironment.WebRootPath;
            string newPath = Path.Combine(webRootPath, folderName);
            StringBuilder sb = new StringBuilder();
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }
            if (file.Length > 0)
            {
                string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                ISheet sheet;
                string fullPath = Path.Combine(newPath, file.FileName);
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                    stream.Position = 0;
                    if (sFileExtension == ".xls")
                    {
                        HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats  
                        sheet = hssfwb.GetSheetAt(1); //get first sheet from workbook  
                    }
                    else
                    {
                        XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format  
                        sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
                    }
                    IRow headerRow = sheet.GetRow(0); //Get Header Row
                    int cellCount = headerRow.LastCellNum;
                    sb.Append("<table class='table'><tr>");
                    for (int j = 0; j < cellCount; j++)
                    {
                        NPOI.SS.UserModel.ICell cell = headerRow.GetCell(j);
                        if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;
                        sb.Append("<th>" + cell.ToString() + "</th>");
                    }
                    sb.Append("</tr>");
                    sb.AppendLine("<tr>");
                    for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                    {
                        IRow row = sheet.GetRow(i);
                        if (row == null) continue;
                        if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
                        for (int j = row.FirstCellNum; j < cellCount; j++)
                        {
                            if (row.GetCell(j) != null)
                                sb.Append("<td>" + row.GetCell(j).ToString() + "</td>");
                        }
                        sb.AppendLine("</tr>");
                    }
                    sb.Append("</table>");
                }
            }
            return this.Content(sb.ToString());
        }

        public async Task<IActionResult> UploadLeave(IFormFile theExcel)
        {
            var user = await _userManager.GetUserAsync(User);


            ExcelPackage excel;

            using (var memorystream = new MemoryStream())
            {
                await theExcel.CopyToAsync(memorystream);
                excel = new ExcelPackage(memorystream);
            }

            var worksheet = excel.Workbook.Worksheets[0];
            var start = worksheet.Dimension.Start;
            var end = worksheet.Dimension.End;
            for (int row = start.Row + 1; row <= end.Row; row++)
            {
                LeaveUpload a = new LeaveUpload
                {
                    //Id= int.Parse(worksheet.Cells[row, 1].Text),
                    Surname = worksheet.Cells[row, 1].Text,
                    FirstName = worksheet.Cells[row, 2].Text,
                    StaffNumber = worksheet.Cells[row, 3].Text,
                    Location = worksheet.Cells[row, 4].Text,
                    NumberOfOutstandingLeaves = worksheet.Cells[row, 5].Text,
                    CurrentLeaveDays = worksheet.Cells[row, 6].Text,
                    TotalLeaves = (Convert.ToInt32(worksheet.Cells[row, 5].Text) + Convert.ToInt32(worksheet.Cells[row, 6].Text)).ToString(),
                    RemainingLeaves = (Convert.ToInt32(worksheet.Cells[row, 5].Text) + Convert.ToInt32(worksheet.Cells[row, 6].Text)).ToString(),
                    //UtilizedLeaves = worksheet.Cells[row, 9].Text ?? "0".ToString()
                };

                a.AddedBy = user.UserName;
                 _context.Add(a);
            };

          await  _context.SaveChangesAsync();
          TempData["test"] = "Upload succesful";
       
          return RedirectToAction(nameof(Index));
        }
        // GET: LeaveUploadController/Details/5
        public IActionResult Details(int id)
        {
            return View();
        }

        // GET: LeaveUploadController/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: LeaveUploadController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: LeaveUploadController/Edit/5
        public IActionResult Edit(int id)
        {
            return View();
        }

        // POST: LeaveUploadController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: LeaveUploadController/Delete/5
        public IActionResult Delete(int id)
        {
            return View();
        }

        // POST: LeaveUploadController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
