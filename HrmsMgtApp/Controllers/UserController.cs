﻿using HrmsMgtApp.Data;
using HrmsMgtApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace HrmsMgtApp.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly IApplicationUser _userService;
        private readonly IEmployeeBio _employeeBio;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public UserController(IApplicationUser userService,IEmployeeBio employeeBio,
                                     UserManager<ApplicationUser> userManager, IWebHostEnvironment hostingEnvironment)
        {
            _userService = userService;
            _employeeBio = employeeBio;
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
        }
        // GET: UserController
        public IActionResult Index()
        {
            return View();
        }

        // GET: UserController/Details/5
        public IActionResult Details(int id)
        {
          var result=  _userService.GetUser(id);

            var employeebio = _employeeBio.GetEmployee(result.Id);

            if (result==null)
            {
                ViewBag.ErrorMessage = $"User with id = {id} cannot be found";
                return View("ErrorPage");
            }


            UserdetailsVm userdetails = new UserdetailsVm()
            {
                User=result,
                BioData=employeebio
            };
            return View(userdetails);
        }

        // GET: UserController/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: UserController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: UserController/Edit/5
        public IActionResult Edit(int id)
        {
            return View();
        }

        // POST: UserController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadImage(IFormFile file)
        {
            var s = await _userManager.GetUserAsync(User);

            if (ModelState.IsValid)
            {
                if (file != null && file.Length > 0)
                {
                    var uploadDir = @"images/Profile";
                    var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                    var extension = Path.GetExtension(file.FileName);
                    var webRootPath = _hostingEnvironment.WebRootPath;
                    fileName = DateTime.UtcNow.ToString("yymmssfff") + fileName + extension;
                    var path = Path.Combine(webRootPath, uploadDir, fileName);
                    await file.CopyToAsync(new FileStream(path, FileMode.Create));
                    s.ProfileImageUrl = "/" + uploadDir + "/" + fileName;
                }

                await _userService.Update(s);

                return RedirectToAction("Details", "User", new { id = s.Id });
            }
            return View();
        }

        // GET: UserController/Delete/5
        public IActionResult Delete(int id)
        {
            return View();
        }

        // POST: UserController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
