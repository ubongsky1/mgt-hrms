﻿using HrmsMgtApp.Data;
using HrmsMgtApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MimeKit;
using MimeKit.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;

namespace HrmsMgtApp.Controllers
{
    [Authorize]
    public class LeavesController : Controller
    {
        private readonly ILeaves _leavesService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILeaveTypes _leaveTypesService;
        private readonly IUnit _unitService;
        private readonly ApplicationDbContext _context;
        private readonly ILeaveUpload _leaveUpload;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public LeavesController(ILeaves leavesService,UserManager<ApplicationUser> userManager,
                                       ILeaveTypes leaveTypesService,IUnit unitService,ApplicationDbContext context,
                                       ILeaveUpload leaveUpload,SignInManager<ApplicationUser> signInManager)
        {
            _leavesService = leavesService;
            _userManager = userManager;
            _leaveTypesService = leaveTypesService;
            _unitService = unitService;
            _context = context;
            _leaveUpload = leaveUpload;
            _signInManager = signInManager;
        }
        // GET: LeavesController

        public async Task<IActionResult> UHLeaveList()
        {
            var user = await _userManager.GetUserAsync(User);

            var result = _leavesService.GetAllLeavesUH(user);

            var indexview = new AdminLeavesIndex()
            {
                TotalLeaves = result.Count(),
                ApprovedRequests=result.Count(result=>result.UHeadStatus==true),
                PendingRequests=result.Count(result=>result.UHeadStatus==null),
                RejectedRequests=result.Count(result=>result.UHeadStatus==false),
                TotalRequests=result.Count(),
                AllLeaves=result
            };


            return View(indexview);
        }

        public async Task<IActionResult> HRLeaveList()
        {
            var user = await _userManager.GetUserAsync(User);

            var result = _leavesService.GetAllLeavesHR();

            var indexView = new HrLeavesIndexVm()
            {
                AllLeaves=result,
                ApprovedRequests=result.Count(res=>res.HRStatus==true),
                PendingRequests=result.Count(res=>res.HRStatus==null),
                RejectedRequests=result.Count(res=>res.HRStatus==false),
                TotalLeaves=result.Count(),
                TotalRequests=result.Count()
            };


            return View(indexView);
        }


        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);

            var check = _leavesService.GetAllLeaves(user);

            return View(check);
        }

        // GET: LeavesController/Details/5
        public IActionResult Details(int id)
        {
            var leaveDetails = _leavesService.GetLeavess(id);
            if (leaveDetails==null)
            {
                ViewBag.ErrorMessage = $"Leaves  with Id={id} cannot be found, Kindly contact the HR or Admin";

                return View("ErrorPage");
            }
            return View(leaveDetails);
        }

        // GET: LeavesController/Create
        public async Task<IActionResult> Create()
        {
            var user = await _userManager.GetUserAsync(User);


             ViewData["LeaveTypeId"] = new SelectList(_leaveTypesService.GetAllLeaves(), "Id", "Type");
             ViewData["UnitsId"] = new SelectList(_unitService.GetAllUnits(), "Id", "Name");

            var NormalUsers = (from u in _context.Users
                              join r in _context.UserRoles on u.Id equals r.UserId
                              join s in _context.Roles on r.RoleId equals s.Id
                              where u.Id==r.UserId && (/*s.Name != "HR" || s.Name != "UnitHead" ||*/ s.Name == "User" /*|| s.Name != "SuperAdmin"*/)
                              select new
                              {
                                  Value=r.UserId,
                                  Text=u.FirstName + ' ' + u.LastName
                              }).ToList();

            ViewData["Users"] = new SelectList(NormalUsers, "Value", "Text");

            var checkuserLeaves = _leaveUpload.GetLeaveUser(user);

            var isApplyAlready = _leavesService.GetAllLeaves(user).LastOrDefault();

         

            if (checkuserLeaves==null)
            {
                ViewBag.ErrorMessage = $"Leaves  for user Name = {user.UserName} cannot be found, Kindly contact the HR or Admin";

                return View("ErrorPage");
            }

            if (isApplyAlready==null)
            {
                ViewBag.TotalLeaves = Convert.ToInt32(checkuserLeaves.TotalLeaves);
                ViewBag.RemainingLeaves = Convert.ToInt32(checkuserLeaves.RemainingLeaves);
                ViewBag.OutstandingLeaves = Convert.ToInt32(checkuserLeaves.NumberOfOutstandingLeaves);
                ViewBag.CurrentLeaves = Convert.ToInt32(checkuserLeaves.CurrentLeaveDays);

                var utilizeLeavess = checkuserLeaves.UtilizedLeaves;

                if (ViewBag.RemainingLeaves <= 0 || ViewBag.RemainingLeaves == null)
                {

                    ViewBag.ErrorMessage = $"Leaves  for user Name = {user.UserName} has been exhausted, Kindly contact the HR or Admin";

                    return View("ErrorPage");
                }

                if (utilizeLeavess == null)
                {
                    ViewBag.UtilizeLeaves = 0;
                }
                else
                {
                    ViewBag.UtilizeLeaves = utilizeLeavess;
                }

                return View();
            }

            if (isApplyAlready.UHeadStatus == null || isApplyAlready.UHeadStatus==false)
            {

                ViewBag.ErrorMessage = $"Leaves  for user Name = {user.UserName} have been previously submitted, Kindly contact the HR or Admin for status";

                return View("ErrorPage");
            }


            ViewBag.TotalLeaves = Convert.ToInt32(checkuserLeaves.TotalLeaves);
            ViewBag.RemainingLeaves = Convert.ToInt32(checkuserLeaves.RemainingLeaves);
            ViewBag.OutstandingLeaves = Convert.ToInt32(checkuserLeaves.NumberOfOutstandingLeaves);
            ViewBag.CurrentLeaves = Convert.ToInt32(checkuserLeaves.CurrentLeaveDays);

            var utilizeLeaves = checkuserLeaves.UtilizedLeaves;

            if (ViewBag.RemainingLeaves <= 0 || ViewBag.RemainingLeaves == null)
            {

                ViewBag.ErrorMessage = $"Leaves  for user Name = {user.UserName} has been exhausted, Kindly contact the HR or Admin";

                return View("ErrorPage");
            }

            if (utilizeLeaves == null)
            {
                ViewBag.UtilizeLeaves = 0;
            }
            else
            {
                ViewBag.UtilizeLeaves = utilizeLeaves;
            }



            return View();

           
        }

        // POST: LeavesController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Obsolete]
        public async Task<IActionResult> Create(Leave model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = await _userManager.GetUserAsync(User);

                    var startDate = Convert.ToDateTime(model.StartDate);
                    var endDate = Convert.ToDateTime(model.EndDate);

                    int weekEnds = NumberOfWeeekends(startDate, endDate);
                    var leaveRequested = (endDate - startDate).Days - weekEnds;

                    var leavetype = _leaveTypesService.GetLeaveType(model.LeaveTypeId);

                    var leaveTypeDays = leavetype.NumberOfDays;

                    var leaveUpload = _leaveUpload.GetLeaveUser(user);
                    var leaveUp = _leaveUpload.GetLeaveUser(user);

                    //if (leavetype.Type== "Annual")
                    //{
                    //    leavetype.NumberOfDays = Convert.ToInt32(leaveUpload.RemainingLeaves);
                    //}
                    if (model.LeaveTypeId != 1)
                    {
                        await _leavesService.Create(model);
                        return RedirectToAction(nameof(Index));
                    }

                    if (Convert.ToInt32(leaveRequested) > Convert.ToInt32(leaveUpload.RemainingLeaves))
                    {
                        ViewBag.ErrorMessage = $"Leaves  for user Name = {user.UserName} has been exhausted or more than requested leave, Kindly contact the HR or Admin";

                        return View("ErrorPage");
                    }


                    leaveUpload.RemainingLeaves = (Convert.ToInt32(leaveUpload.RemainingLeaves) - Convert.ToInt32(leaveRequested)).ToString();
                    leaveUpload.UtilizedLeaves = (Convert.ToInt32(leaveUpload.UtilizedLeaves) + Convert.ToInt32(leaveRequested)).ToString();

                    model.LeaveRemaining = Convert.ToInt32(leaveUpload.RemainingLeaves);
                    model.LeaveUtilized = Convert.ToInt32(leaveUpload.UtilizedLeaves);


                    await _leaveUpload.Update(leaveUpload);

                    await _leavesService.Create(model);


                    //var checkUnitHead = (from U in _context.Users
                    //                     join Ur in _context.UserRoles on U.Id equals Ur.UserId
                    //                     join R in _context.Roles on Ur.RoleId equals R.Id
                    //                     where U.Id==Ur.UserId && U.UnitId==user.UnitId
                    //                     select new
                    //                     {

                    //                     });

                    var Allusers = await _userManager.GetUsersInRoleAsync("UnitHead");



                    var UnitHead = Allusers.Where(x => x.UnitId == user.UnitId).FirstOrDefault();
                    var toName = UnitHead.FirstName.ToString();
                    var toEmail = UnitHead.Email.ToString();
                    var to = user.FirstName.ToString();

                    //instantiate a new MimeMessage
                    var message = new MimeMessage();
                    //Setting the To e-mail address
                    message.To.Add(new MailboxAddress(toName, toEmail));

                    var tt = "ubong.umoh@junuvo.ng";
                    //var tt = "humanresources@novohealthafrica.org";
                    message.Bcc.Add(new MailboxAddress(tt));
                    //Setting the From e-mail address
                    message.From.Add(new MailboxAddress("E-mail From Hrms", "ubsky1@gmail.com"));
                    //E-mail subject 
                    message.Subject = "New Service Request Added";
                    //E-mail message body
                    message.Body = new TextPart(TextFormat.Html)
                    {
                        Text = "<img src='https://picsum.photos/200/300' width='400' height='100'>" +
                       "<br/><br/><br/><br/>" +
                    "<p>Dear Colleague,</p>" +

                   "<p> This is to notify you that a new leave request has been sent by " + user.FirstName + " " + user.LastName +

                   "<br/>on </a>" + model.Created + ".</p>" +
                   "Unit Head can see more information on the leave request by Clicking <a href='https://localhost:44307/leaves/Details/" + model.Id + "'>Here</a>" +




                                                  "<br/>Regards<br/>" +


                                                   "<img src='https://picsum.photos/200/300' width='400' height='100'>"
                    };

                    //Configure the e - mail
                    using (var emailClient = new SmtpClient())
                    {

                        emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                        emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                        emailClient.Connect("smtp.gmail.com", 587, false);
                        emailClient.Authenticate("ubsky1@gmail.com", "PETROLEUM11@");
                        emailClient.Send(message);
                        emailClient.Disconnect(true);
                    }

                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = $"Kindly check your internet connection but the request has been sent";
                    return View("ErrorPage");
                }

                if (_signInManager.IsSignedIn(User)&& User.IsInRole("User"))
                {
                    return RedirectToAction("Index", "Leaves");
                }
                else if (_signInManager.IsSignedIn(User) && User.IsInRole("UnitHead"))
                {
                    return RedirectToAction("UHLeaveList", "Leaves");
                }
                else
                {
                    return RedirectToAction("HRLeaveList", "Leaves");
                }

                
            }

            ViewBag.TotalLeaves = Convert.ToInt32(model.TotalLeave);
            ViewBag.RemainingLeaves = Convert.ToInt32(model.LeaveRemaining);
       
          

            ViewData["LeaveTypeId"] = new SelectList(_leaveTypesService.GetAllLeaves(), "Id", "Type", model.LeaveTypeId);
            ViewData["UnitsId"] = new SelectList(_unitService.GetAllUnits(), "Id", "Name", model.UnitsId);
            var NormalUsers = (from u in _context.Users
                               join r in _context.UserRoles on u.Id equals r.UserId
                               join ss in _context.Roles on r.RoleId equals ss.Id
                               where u.Id == r.UserId && (/*s.Name != "HR" || s.Name != "UnitHead" ||*/ ss.Name == "User" /*|| s.Name != "SuperAdmin"*/)
                               select new
                               {
                                   Value = r.UserId,
                                   Text = u.FirstName + ' ' + u.LastName
                               }).ToList();

            ViewData["Users"] = new SelectList(NormalUsers, "Value", "Text", model.UserId);

            return View(model);
        }


        private static int NumberOfWeeekends(DateTime startDate, DateTime endDate)
        {
            int countWeekend = 0;



            TimeSpan diff = endDate - startDate;
            int days = diff.Days;
            for (var i = 0; i <= days; i++)
            {
                var testDate = startDate.AddDays(i);
                switch (testDate.DayOfWeek)
                {
                    case DayOfWeek.Saturday:
                    case DayOfWeek.Sunday:
                        //Console.WriteLine(testDate.ToShortDateString());
                        countWeekend++;
                        break;
                }

            }

            return countWeekend;
        }


        [HttpPost]
        [Obsolete]
        public async Task<IActionResult> ApproveRequest(int id)
        {
            var user = await _userManager.GetUserAsync(User);

            var isUnitHead = await _userManager.IsInRoleAsync(user, "UnitHead");

            var isHr = await _userManager.IsInRoleAsync(user, "HR");

            if (isUnitHead==true)
            {
                try
                {
                    var Check = _leavesService.GetLeavess(id);

                    Check.UHeadStatus = true;

                    await _leavesService.Update(Check);


                    var messageUser = Check.EmployeeEmail;

                    var UnitHeadEmail = user.Email;

                    var message = new MimeMessage();
                    //Setting the To e-mail address
                    message.To.Add(new MailboxAddress(messageUser));

                    var tt = "ubong.umoh@junuvo.ng";
                    //var tt = "humanresources@novohealthafrica.org";
                    message.Bcc.Add(new MailboxAddress(tt));
                    //Setting the From e-mail address
                    message.From.Add(new MailboxAddress("E-mail From Hrms", "ubsky1@gmail.com"));
                    //E-mail subject 
                    message.Subject = "New Leave Request";
                    //E-mail message body
                    message.Body = new TextPart(TextFormat.Html)
                    {
                        Text = "<img src='https://picsum.photos/200/300' width='400' height='100'>" +
                               "<br/><br/><br/><br/>" +
                            "<p>Dear</p>" + Check.AddedBy + "," +

                           "<p> This is to notify you that the leave request you sent on " + Check.Created + "has been approved by your Unit Head but awaiting approval by the Head of Human Resource Unit." +
                           "For more inforation on the leave request, Click <a href='https://localhost:44307/leaves/Details/" + Check.Id + "'>Here</a>" +





                                                          "<br/>Regards<br/>" +


                                                           "<img src='https://picsum.photos/200/300' width='400' height='100'>"
                    };

                    //Configure the e - mail
                    using (var emailClient = new SmtpClient())
                    {

                        emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                        emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                        emailClient.Connect("smtp.gmail.com", 587, false);
                        emailClient.Authenticate("ubsky1@gmail.com", "PETROLEUM11@");
                        emailClient.Send(message);
                        emailClient.Disconnect(true);
                    }
                }
                catch ( Exception ex)
                {

                    ViewBag.ErrorMessage = $"Kindly check your internet connection but the request has been sent";
                    return View("ErrorPage");
                }


                return RedirectToAction("UHLeaveList", "Leaves");
            }
            else
            {
                try
                {
                    var Check = _leavesService.GetLeavess(id);

                    Check.HRStatus = true;

                    await _leavesService.Update(Check);

                    var messageUser = Check.EmployeeEmail;

                    var HrHeadEmail = user.Email;

                    var message = new MimeMessage();
                    //Setting the To e-mail address
                    message.To.Add(new MailboxAddress(messageUser));

                    var tt = "ubong.umoh@junuvo.ng";
                    //var tt = "humanresources@novohealthafrica.org";
                    message.Bcc.Add(new MailboxAddress(tt));
                    //Setting the From e-mail address
                    message.From.Add(new MailboxAddress("E-mail From Hrms", "ubsky1@gmail.com"));
                    //E-mail subject 
                    message.Subject = "New Leave Request";
                    //E-mail message body
                    message.Body = new TextPart(TextFormat.Html)
                    {
                        Text = "<img src='https://picsum.photos/200/300' width='400' height='100'>" +
                               "<br/><br/><br/><br/>" +
                            "<p>Dear</p>" + Check.AddedBy + "," +

                           "<p> This is to notify you that the leave request you sent on " + Check.Created + "has been approved by your Hr." +
                           "For more inforation on the leave request, Click <a href='https://localhost:44307/leaves/Details/" + Check.Id + "'>Here</a>" +





                                                          "<br/>Regards<br/>" +


                                                           "<img src='https://picsum.photos/200/300' width='400' height='100'>"
                    };

                    //Configure the e - mail
                    using (var emailClient = new SmtpClient())
                    {

                        emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                        emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                        emailClient.Connect("smtp.gmail.com", 587, false);
                        emailClient.Authenticate("ubsky1@gmail.com", "PETROLEUM11@");
                        emailClient.Send(message);
                        emailClient.Disconnect(true);
                    }
                }
                catch ( Exception ex)
                {

                    ViewBag.ErrorMessage = $"Kindly check your internet connection but the request has been sent";
                    return View("ErrorPage");
                }

                return RedirectToAction("HRLeaveList", "Leaves");
            }
            
        
         
        }

        [HttpPost]
        [Obsolete]
        public async Task<IActionResult> RejectRequest(int id, string LeaveRequestStatus=null)
        {
            var user = await _userManager.GetUserAsync(User);

            var isUnitHead = await _userManager.IsInRoleAsync(user, "UnitHead");

            var isHr = await _userManager.IsInRoleAsync(user, "HR");

            if (isUnitHead == true)
            {
                try
                {
                    var Check = _leavesService.GetLeavess(id);

                    Check.UHeadStatus = false;
                    Check.LeaveRequestStatus = LeaveRequestStatus;

                    await _leavesService.Update(Check);

                    var messageUser = Check.EmployeeEmail;

                    var UnitHeadEmail = user.Email;

                    var message = new MimeMessage();
                    //Setting the To e-mail address
                    message.To.Add(new MailboxAddress(messageUser));

                    var tt = "ubong.umoh@junuvo.ng";
                    //var tt = "humanresources@novohealthafrica.org";
                    message.Bcc.Add(new MailboxAddress(tt));
                    //Setting the From e-mail address
                    message.From.Add(new MailboxAddress("E-mail From Hrms", "ubsky1@gmail.com"));
                    //E-mail subject 
                    message.Subject = "New Leave Request";
                    //E-mail message body
                    message.Body = new TextPart(TextFormat.Html)
                    {
                        Text = "<img src='https://picsum.photos/200/300' width='400' height='100'>" +
                               "<br/><br/><br/><br/>" +
                            "<p>Dear</p>" + Check.AddedBy + "," +

                           "<p> This is to notify you that the leave request you sent on " + Check.Created + "has been Disapproved by your Unit Head." +
                           "For more inforation on the leave request, Click <a href='https://localhost:44307/leaves/Details/" + Check.Id + "'>Here</a>" +





                                                          "<br/>Regards<br/>" +


                                                           "<img src='https://picsum.photos/200/300' width='400' height='100'>"
                    };

                    //Configure the e - mail
                    using (var emailClient = new SmtpClient())
                    {

                        emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                        emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                        emailClient.Connect("smtp.gmail.com", 587, false);
                        emailClient.Authenticate("ubsky1@gmail.com", "PETROLEUM11@");
                        emailClient.Send(message);
                        emailClient.Disconnect(true);
                    }

                }
                catch ( Exception ex)
                {

                    ViewBag.ErrorMessage = $"Kindly check your internet connection but the request has been sent";
                    return View("ErrorPage");
                }
                return RedirectToAction("UHLeaveList", "Leaves");
            }
            else
            {
                try
                {
                    var Check = _leavesService.GetLeavess(id);

                    Check.HRStatus = false;
                    Check.LeaveRequestStatus = LeaveRequestStatus;
                    await _leavesService.Update(Check);
                    var messageUser = Check.EmployeeEmail;

                    var UnitHeadEmail = user.Email;

                    var message = new MimeMessage();
                    //Setting the To e-mail address
                    message.To.Add(new MailboxAddress(messageUser));

                    var tt = "ubong.umoh@junuvo.ng";
                    //var tt = "humanresources@novohealthafrica.org";
                    message.Bcc.Add(new MailboxAddress(tt));
                    //Setting the From e-mail address
                    message.From.Add(new MailboxAddress("E-mail From Hrms", "ubsky1@gmail.com"));
                    //E-mail subject 
                    message.Subject = "New Leave Request";
                    //E-mail message body
                    message.Body = new TextPart(TextFormat.Html)
                    {
                        Text = "<img src='https://picsum.photos/200/300' width='400' height='100'>" +
                               "<br/><br/><br/><br/>" +
                            "<p>Dear</p>" + Check.AddedBy + "," +

                           "<p> This is to notify you that the leave request you sent on " + Check.Created + "has been Disapproved by your Hr Head." +
                           "For more inforation on the leave request, Click <a href='https://localhost:44307/leaves/Details/" + Check.Id + "'>Here</a>" +





                                                          "<br/>Regards<br/>" +


                                                           "<img src='https://picsum.photos/200/300' width='400' height='100'>"
                    };

                    //Configure the e - mail
                    using (var emailClient = new SmtpClient())
                    {

                        emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                        emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                        emailClient.Connect("smtp.gmail.com", 587, false);
                        emailClient.Authenticate("ubsky1@gmail.com", "PETROLEUM11@");
                        emailClient.Send(message);
                        emailClient.Disconnect(true);
                    }

                }
                catch ( Exception ex)
                {

                    ViewBag.ErrorMessage = $"Kindly check your internet connection but the request has been sent";
                    return View("ErrorPage");
                }
                return RedirectToAction("HRLeaveList", "Leaves");
            }



        }

        // GET: LeavesController/Edit/5
        [HttpGet]
        public IActionResult Edit(int id)
        {
            var s = _leavesService.GetLeavess(id);
            if (s==null)
            {
                ViewBag.ErrorMessage = $"Leaves  with id = {id} could not be found, Kindly contact the HR or Admin";

                return View("ErrorPage");
            }

            ViewData["LeaveTypeId"] = new SelectList(_leaveTypesService.GetAllLeaves(), "Id", "Type",s.LeaveTypeId);
            ViewData["UnitsId"] = new SelectList(_unitService.GetAllUnits(), "Id", "Name",s.UnitsId);
            var NormalUsers = (from u in _context.Users
                               join r in _context.UserRoles on u.Id equals r.UserId
                               join ss in _context.Roles on r.RoleId equals ss.Id
                               where u.Id == r.UserId && (/*s.Name != "HR" || s.Name != "UnitHead" ||*/ ss.Name == "User" /*|| s.Name != "SuperAdmin"*/)
                               select new
                               {
                                   Value = r.UserId,
                                   Text = u.FirstName + ' ' + u.LastName
                               }).ToList();

            ViewData["Users"] = new SelectList(NormalUsers, "Value", "Text",s.UserId);
            return View(s);
        }

        // POST: LeavesController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Obsolete]
        public async Task<IActionResult> Edit(Leave model)
        {
            if (ModelState.IsValid)
            {
                await _leavesService.Update(model);

                //if (model.UHeadStatus==true)
                //{
                //    var user = await _userManager.GetUserAsync(User);

                //    var adminUser = (
                //                    from U in _context.Users
                //                    join R in _context.UserRoles on U.Id equals R.UserId
                //                    join UR in _context.Roles on R.RoleId equals UR.Id
                //                    where U.Id == R.UserId && UR.Name== "UnitHead" && U.UnitId==user.UnitId
                //                    select new
                //                    {
                //                       U.UserName,
                //                       U.FirstName,
                //                       U.LastName
                //                    }).FirstOrDefault();

                //    string userEmail = model.EmployeeEmail;

                //    var approvee = user.Email;

                //    var message = new MimeMessage();
                //    //Setting the To e-mail address
                //    message.To.Add(new MailboxAddress(userEmail));


                //    //var tt = "ubsky1@gmail.com";
                //    //message.Bcc.Add(new MailboxAddress(tt));
                //    //Setting the From e-mail address
                //    message.From.Add(new MailboxAddress("E-mail From Hrms", "ubsky1@gmail.com"));
                //    //E-mail subject 
                //    message.Subject = "Confirm Registration";
                //    //E-mail message body
                //    message.Body = new TextPart(TextFormat.Html)
                //    {
                //        Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                //           "<br/><br/><br/><br/>" +
                //        "<p>Dear</p>" + userEmail + "," +

                //       "<p> This is to notify you that the leave request you sent on " + model.Created + "has been approved by your Unit Head but awaiting approval by the Head of Human Resource Unit." +
                //       "For more inforation on the leave request, Click <a href='https://localhost:44307/Leaves/Details/" + model.Id + "'>Here</a>" +





                //                                      "<br/>Regards<br/>" +


                //                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                //    };

                //    //Configure the e-mail
                //    using (var emailClient = new SmtpClient())
                //    {

                //        emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                //        emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                //        emailClient.Connect("smtp.gmail.com", 587, false);
                //        emailClient.Authenticate("ubsky1@gmail.com", "PETROLEUM11@");
                //        emailClient.Send(message);
                //        emailClient.Disconnect(true);
                //    }
                //}
                return RedirectToAction(nameof(Index));
            }

            ViewData["LeaveTypeId"] = new SelectList(_leaveTypesService.GetAllLeaves(), "Id", "Type", model.LeaveTypeId);
            ViewData["UnitsId"] = new SelectList(_unitService.GetAllUnits(), "Id", "Name", model.UnitsId);
            var NormalUsers = (from u in _context.Users
                               join r in _context.UserRoles on u.Id equals r.UserId
                               join ss in _context.Roles on r.RoleId equals ss.Id
                               where u.Id == r.UserId && (/*s.Name != "HR" || s.Name != "UnitHead" ||*/ ss.Name == "User" /*|| s.Name != "SuperAdmin"*/)
                               select new
                               {
                                   Value = r.UserId,
                                   Text = u.FirstName + ' ' + u.LastName
                               }).ToList();

            ViewData["Users"] = new SelectList(NormalUsers, "Value", "Text", model.UserId);

            return View(model);
        }

        // GET: LeavesController/Delete/5
        [HttpGet]
        public IActionResult Delete(int id)
        {
            var check = _leavesService.GetLeavess(id);
            if (check==null)
            {
                ViewBag.ErrorMessage = $"Leaves  with id = {id} could not be found, Kindly contact the HR or Admin";

                return View("ErrorPage");
            }
            return View(check);
        }

        // POST: LeavesController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(Leave model)
        {
            var user = await _userManager.GetUserAsync(User);
            var check = _leavesService.GetLeavess(model.Id);

            if (check.LeaveTypeId !=1)
            {
               await _leavesService.Delete(model.Id);
                return RedirectToAction(nameof(Index));
            }

            var FindLeave = _leavesService.GetLeavess(model.Id);
            var startDate = Convert.ToDateTime(FindLeave.StartDate);
            var endDate = Convert.ToDateTime(FindLeave.EndDate);

            int weekEnds = NumberOfWeeekends(startDate, endDate);
            var leaveRequested = (endDate - startDate).Days - weekEnds;

            var Leaveup = _leaveUpload.GetLeaveUser(user);

            Leaveup.RemainingLeaves =(Convert.ToInt32( Leaveup.RemainingLeaves) + leaveRequested).ToString();
            Leaveup.UtilizedLeaves =(Convert.ToInt32(Leaveup.UtilizedLeaves) - Convert.ToInt32(leaveRequested)).ToString();

           await _leaveUpload.Update(Leaveup);

            await _leavesService.Delete(model.Id);
        

            return RedirectToAction(nameof(Index));
        }
    }
}
