﻿using HrmsMgtApp.Data;
using HrmsMgtApp.Models;
using HrmsMgtApp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace HrmsMgtApp.Controllers
{
    [Authorize]
    public class EmployeeBioController : Controller
    {
        private readonly IEmployeeBio _employeeBioService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public EmployeeBioController(IEmployeeBio employeeBioService, UserManager<ApplicationUser> userManager,
                                          IWebHostEnvironment hostingEnvironment)
        {
            _employeeBioService = employeeBioService;
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
        }
        // GET: EmployeeBioController
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);

            var Userbio = _employeeBioService.GetEmployee(user.Id);

            if (Userbio==null)
            {
                ViewBag.ErrorMessage = $"userbio with id = {user.Id} cannot be found, Kindly complete your registration";
                return View("ErrorPage");
            }
            return View(Userbio);
        }

        // GET: EmployeeBioController/Details/5
        public IActionResult Details(int id)
        {
            return View();
        }

        // GET: EmployeeBioController/Create
        public IActionResult Create()
        {
            var s = new EmployeeBioVm();
            return View(s);
        }

        // POST: EmployeeBioController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EmployeeBioVm model)
        {
            if (ModelState.IsValid)
            {
                EmployeeBioData data = new EmployeeBioData()
                {
                    EmployeeId=model.EmployeeId,
                    FirstName=model.FirstName,
                    LastName=model.LastName,
                    BloodGroup=model.BloodGroup,
                    DOB=model.DOB,
                    CUG=model.CUG,
                    EmployeeNo=model.EmployeeNo,
                    ModeOfEmployment=model.ModeOfEmployment,
                    DateOfEmployment=model.DateOfEmployment,
                    YearsOfExperience=model.YearsOfExperience,
                    Designation=model.Designation,
                    MaritalStatus=model.MaritalStatus,
                    Address=model.Address,
                    CountryOfOrigin=model.CountryOfOrigin,
                    BeneficiaryName=model.BeneficiaryName,
                    BeneficiaryAddress=model.BeneficiaryAddress,
                    BeneficiaryNo=model.BeneficiaryNo,
                    EmergencyName=model.EmergencyName,
                    EmergencyAddress=model.EmergencyAddress,
                    Lgas=model.Lgas,
                    States=model.States,
                    Region=model.Region,
                    City=model.City,
                    Academic=model.Academic,
                    AcademicDescription=model.AcademicDescription,
                    Professional=model.Professional,
                    ProfessionalDescription=model.ProfessionalDescription,
                    Institution=model.Institution,
                    Duration=model.Duration,
                    StartDate=model.StartDate,
                    EndDate=model.EndDate,
                    AddedBy=model.AddedBy,
                    Gender= (Gender)model.Sex,
                    CreatedDate=model.CreatedDate
                };



                if (model.Image != null && model.Image.Length > 0)
                {
                    var uploadDir = @"images/Cert";
                    var fileName = Path.GetFileNameWithoutExtension(model.Image.FileName);
                    var extension = Path.GetExtension(model.Image.FileName);
                    var webRootPath = _hostingEnvironment.WebRootPath;
                    fileName = model.FirstName + fileName + extension;
                    var path = Path.Combine(webRootPath, uploadDir, fileName);
                    await model.Image.CopyToAsync(new FileStream(path, FileMode.Create));
                    data.Image = "/" + uploadDir + "/" + fileName;
                }

                await   _employeeBioService.Create(data);
             return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        // GET: EmployeeBioController/Edit/5
        public IActionResult Edit(int id)
        {
            var result = _employeeBioService.GetEmployeeBio(id);

            if (result==null)
            {
                ViewBag.ErrorMessage = $"userbio with id = {id} cannot be found";
                return View("ErrorPage");
            }


            EmployeeEditBioVm bioVm = new EmployeeEditBioVm()
            {
                Id=result.Id,
                EmployeeId=result.EmployeeId,
                FirstName=result.FirstName,
                LastName=result.LastName,
                BloodGroup=result.BloodGroup,
                DOB=result.DOB,
                CUG=result.CUG,
                EmployeeNo=result.EmployeeNo,
                ModeOfEmployment=result.ModeOfEmployment,
                DateOfEmployment=result.DateOfEmployment,
                YearsOfExperience=result.YearsOfExperience,
                Designation=result.Designation,
                MaritalStatus=result.MaritalStatus,
                Address=result.Address,
                CountryOfOrigin=result.CountryOfOrigin,
                BeneficiaryName=result.BeneficiaryName,
                BeneficiaryNo=result.BeneficiaryNo,
                BeneficiaryAddress=result.BeneficiaryAddress,
                EmergencyName=result.EmergencyName,
                EmergencyNo=result.EmergencyNo,
                EmergencyAddress=result.EmergencyAddress,
                Lgas=result.Lgas,
                States=result.States,
                Region=result.Region,
                City=result.City,
                Academic=result.Academic,
                AcademicDescription=result.AcademicDescription,
                Professional=result.Professional,
                ProfessionalDescription=result.ProfessionalDescription,
                Institution=result.Institution,
                ExistingPhotoPath=result.Image,
                Duration=result.Duration,
                StartDate=result.StartDate,
                EndDate=result.EndDate,
                AddedBy=result.AddedBy,
                CreatedDate=result.CreatedDate,
                Sex= (Sex)result.Gender,
                
            };

            return View(bioVm);
        }

        // POST: EmployeeBioController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EmployeeEditBioVm model)
        {

            if (ModelState.IsValid)
            {
                var s = _employeeBioService.GetEmployeeBio(model.Id);

                if (s == null)
                {
                    ViewBag.ErrorMessage = $"userbio with id = {s.Id} cannot be found";
                    return View("ErrorPage");
                }

                s.Id = model.Id;
                s.EmployeeId = model.EmployeeId;
                s.FirstName = model.FirstName;
                s.LastName = model.LastName;
                s.BloodGroup = model.BloodGroup;
                s.DOB = model.DOB;
                s.CUG = model.CUG;
                s.EmployeeNo = model.EmployeeNo;
                s.ModeOfEmployment = model.ModeOfEmployment;
                s.DateOfEmployment = model.DateOfEmployment;
                s.YearsOfExperience = model.YearsOfExperience;
                s.Designation = model.Designation;
                s.MaritalStatus = model.MaritalStatus;
                s.Address = model.Address;
                s.CountryOfOrigin = model.CountryOfOrigin;
                s.BeneficiaryName = model.BeneficiaryName;
                s.BeneficiaryNo = model.BeneficiaryNo;
                s.BeneficiaryAddress = model.BeneficiaryAddress;
                s.EmergencyName = model.EmergencyName;
                s.EmergencyNo = model.EmergencyNo;
                s.EmergencyAddress = model.EmergencyAddress;
                s.Lgas = model.Lgas;
                s.States = model.States;
                s.Region = model.Region;
                s.City = model.City;
                s.Academic = model.Academic;
                s.AcademicDescription = model.AcademicDescription;
                s.Professional = model.Professional;
                s.ProfessionalDescription = model.ProfessionalDescription;
                s.Institution = model.Institution;
                //s.Image = model.ExistingPhotoPath;
                s.Duration = model.Duration;
                s.StartDate = model.StartDate;
                s.EndDate = model.EndDate;
                s.AddedBy = model.AddedBy;
                s.Gender = (Gender)model.Sex;

               

                if (model.Image != null && model.Image.Length > 0)
                {
                    if (model.ExistingPhotoPath != null)
                    {
                        string filepath = Path.Combine(_hostingEnvironment.WebRootPath, model.ExistingPhotoPath);
                        System.IO.File.Delete(filepath);
                    }
                    var uploadDir = @"images/Cert";
                    var fileName = Path.GetFileNameWithoutExtension(model.Image.FileName);
                    var extension = Path.GetExtension(model.Image.FileName);
                    var webRootPath = _hostingEnvironment.WebRootPath;
                    fileName = model.FirstName + fileName + extension;
                    var path = Path.Combine(webRootPath, uploadDir, fileName);
                    await model.Image.CopyToAsync(new FileStream(path, FileMode.Create));
                    s.Image = "/" + uploadDir + "/" + fileName;
                }


                await _employeeBioService.Update(s);
                return RedirectToAction("Index", "EmployeeBio");
            }
        
            return View(model);
        }

        // GET: EmployeeBioController/Delete/5
        public IActionResult Delete(int id)
        {
            return View();
        }

        // POST: EmployeeBioController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
