﻿using HrmsMgtApp.Data;
using HrmsMgtApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HrmsMgtApp.Controllers
{
    [Authorize(Roles = "UnitHead, DeptHead, Admin, SuperAdmin,HR")]
    public class UnitController : Controller
    {
        private readonly IUnit _unit;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public UnitController(IUnit unit,UserManager<ApplicationUser> userManager,
                                       SignInManager<ApplicationUser> signInManager)
        {
            _unit = unit;
            _userManager = userManager;
            _signInManager = signInManager;
        }
        // GET: UnitController
        public IActionResult Index()
        {
            var result = _unit.GetAllUnits();

            return View(result);
        }


        public IActionResult Info(int id)
        {
          var s=  _unit.GetUnit(id);

            var result = s.Users;

            var users = new UnitUsers()
            {
                Name = s.Name,
                Id=s.Id,
                Users=result
            };

            return View(users);
        }

        // GET: UnitController/Details/5
        public IActionResult Details(int id)
        {

            var result = _unit.GetUnit(id);
            if (result==null)
            {
                ViewBag.ErrorMessage = $"Department with id = {id} cannot be found";
                return View("ErrorPage");
            }


            return View(result);
        }

        // GET: UnitController/Create
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        // POST: UnitController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Unit model)
        {
            if (ModelState.IsValid)
            {
              await  _unit.Create(model);
              return  RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: UnitController/Edit/5
        public IActionResult Edit(int id)
        {
          var result=  _unit.GetUnit(id);
            if (result==null)
            {
                ViewBag.ErrorMessage = $"Unit with id = {id} cannot be found";
                return View("ErrorPage");
            }
            return View(result);
        }

        // POST: UnitController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Unit model)
        {
            if (ModelState.IsValid)
            {
              await  _unit.Edit(model);
                return RedirectToAction(nameof(Index));
            }
           
            return View(model);
        }

        // GET: UnitController/Delete/5
        public IActionResult Delete(int id)
        {
          var result=  _unit.GetUnit(id);

            if (result==null)
            {
                ViewBag.ErrorMessage = $"Unit with id = {id} cannot be found";
                return View("ErrorPage");
            }
            return View(result);
        }

        // POST: UnitController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
